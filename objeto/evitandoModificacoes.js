// Object.preventExtensions

const produto = Object.preventExtensions({
    nome: 'Qualquer',
    preco: 1.99,
    tag: 'promoção'
})
console.log('Extensível:', Object.preventExtensions(produto))

produto.nome = 'Borracha'
produto.descricao = 'Borracha escolar branca'
delete produto.tag
console.log(produto)

// Object.seal

const pessoa = {
    nome: 'Juliana',
    idade: 35
}

Object.seal(pessoa)
console.log('Selado:', Object.isSealed(pessoa))

pessoa.sobrenome = 'Silva'
delete pessoa.nome
pessoa.idade = 29
console.log(pessoa)

// Object.freeze => selado + valores constantes

const celular = {
    modelo: 'iPhone X',
    ano: 2017
}

Object.freeze(celular)
console.log('Congelado:', Object.isFrozen(celular))

celular.cor = 'Preto'
delete celular.display
celular.modelo = 'iPhone XS Pro'
console.log(celular)