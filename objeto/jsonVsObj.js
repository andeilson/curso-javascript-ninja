const obj = {
    a: 1,
    b: 2,
    c: 3,
    soma() {
        return a + b + c
    }
}

console.log(JSON.stringify(obj)) // forma correta
// console.log(JSON.parse("{a: 1, b: 2, c: 3}")) // forma errada
// console.log(JSON.parse("{'a': 1, 'b': 2, 'c': 3}")) // forma errada
console.log(JSON.parse('{"a": 1, "b": 2, "c": 3}')) // forma correta
console.log(JSON.parse('{"a": 1.7, "b": "string", "c": true, "d": {}, "e": [] }')) // forma correta