/* 
    Objetos e jSON não são a mesma coisa.
    Em um objeto,
    você pode criar ele de forma vazia,
    e começar a atribuir valores 
    para ele
*/

const prod1 = {}
prod1.nome = 'Celular Ultra Mega';
prod1.preco = 4998.90
prod1['Desconto Legal'] = 0.40
console.log(prod1)

/* 
    Dessa forma, você utiliza
    anotação literal. É possível
    ter um objeto dentro de um objeto.
*/
const prod2 = {
    nome: 'Andeilson Ferreira',
    idade: 27,
    profissão: 'Programador Front-end',
    cidade: 'Brasilia',
    obj: {
        blabla: 1,
        obj: {
            blabla: 2
        }
    }
}
console.log(prod2)

/* 
    Como ficaria em
    jSON. jSON é
    texto
*/

'{"nome": "Andeilson Ferreira", "idade": 27, "profissão": "Programador Front-end", "cidade": "Brasilia"}'

