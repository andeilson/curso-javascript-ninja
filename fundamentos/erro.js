function tratarErroELancar(erro) {
    // throw new Error ('...')
    // throw 10
    // throw true
    // throw 'mensagem'
    throw {
        nome: erro.name,
        msg: erro.message,
        date: new Date
    }
}

function imprimirNomeGritado(obj) {
    try { // utilize em um bloco que pode causar algum tipo de erro
        console.log(obj.name.toUpperCase() + '!!!')
    } catch (e) { // recebe os dados do erro caso ocorra
        tratarErroELancar(e)
    } finally { // lança algo mesmo que o erro persista
        console.log('final')
    }
}

const obj = {
    nome: 'Roberto'
}
imprimirNomeGritado(obj)