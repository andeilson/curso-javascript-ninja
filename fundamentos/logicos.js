/* 
    V e V => Verdadeiro
    V e F => Falso
    F e qualquer coisa => Falso

    ############

    ||

    V || V => Verdadeiro
    F || V => Verdadeiro
    F || F => Falso

    Xor
    Os itens não podem ser iguais.
    Se os resultados forem diferentes ele
    retorna verdadeiro.

    V xor V => Falso
    V xor F => Verdadeiro
    F xor V => Verdadeiro
    F xor F => Falso

    !v => F
    !f => V

*/  

function comprar(trabalho1, trabalho2) {
    const comprarSorvete = trabalho1 || trabalho2
    const comprarTvDe50 = trabalho1 && trabalho2
    // const comprarTvDe32 = !!(trabalho1 ^ trabalho2) // bitwise xor
    const comprarTvDe32 = trabalho1 != trabalho2
    const manterSaudavel = !comprarSorvete

    return { comprarSorvete, comprarTvDe50, comprarTvDe32, manterSaudavel }

}

// console.log(comprar(true, true))
// console.log(comprar(true, false))
// console.log(comprar(false, true))
console.log(comprar(false, false))