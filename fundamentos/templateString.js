const nome = 'Rebeca'
const concatenacao = 'Olá ' + nome + '!'

/*
    Através do template string
    é possível concatenar
    utilizando quebra de linhas
*/    
const template = `
    Olá
    ${nome}!`

console.log(concatenacao, template);

/*
    Realizando expressões
    com o template string
*/    
console.log(`1 + 1 = ${1 + 1}`);

const frases = 'vamos ficar rico, porra!'
const up = texto => texto.toUpperCase()
console.log(`Ei... ${up('cuidado')}!`)
// console.log(frases.toUpperCase())
