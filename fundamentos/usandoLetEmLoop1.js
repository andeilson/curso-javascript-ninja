/* 
    Estará visível apenas dentro
    do bloco, pois se trata de let, e 
    não var
*/    

for (let i = 0; i < 10; i++) {
    console.log(i)
}