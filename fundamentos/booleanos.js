let isAtivo = false
console.log(isAtivo)

isAtivo = true
console.log(isAtivo)

/*
    Aqui, iremos utilizar o conceito
    da negação ou da afirmação
    Sendo = !isAtivo => false
    !!isAtivo => true
*/

isAtivo = 1
console.log(!!isAtivo)
// console.log(!!isAtivo)

console.log('os booleanos verdadeiros...')
console.log(!!3)
console.log(!!-1)
console.log(!!' ')
console.log(!![])
console.log(!!{})
console.log(!!Infinity)
console.log(!!(isAtivo = true))

console.log('os booleanos falsos...')
console.log(!!0)
console.log(!!'')
console.log(!!null)
console.log(!!NaN)
console.log(!!undefined)
console.log(!!(isAtivo = false))

console.log('para finalizar');
console.log(!!('' || null || 0 || ' '))

/* 
    retornar os verdadeiros
    o simbolo || significa 'ou'
*/
console.log(('' || null || 0 || '123'))

let nome = ''
console.log(nome || 'Desconhecido')