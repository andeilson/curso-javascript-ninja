// novo recurso do ES2015
// a partir de um objeto

const pessoa = {
    nome: 'Ana',
    idade: 20,
    endereco: {
        logradouro: 'Rua ABC',
        numero: 1000
    }
}

/*
    Tire os dados da const pessoa
    os dados nome e idade
*/
const {
    nome,
    idade
} = pessoa
console.log(nome, idade)

/*
    Puxar os dados através
    de uma nova declaração
    de variável
*/
const {
    nome: n,
    idade: i
} = pessoa
console.log(n, i)

/*
    Setar diretamente uma nova
    variável
*/
const {
    sobrenome,
    bemHumorada = true
} = pessoa
console.log(sobrenome, bemHumorada)

/*
    Puxar os dados dentro
    da base endereço
*/
const {
    endereco: {
        logradouro,
        numero,
        cep
    }
} = pessoa
console.log(logradouro, numero, cep)

/*
    Não tenta puxar dentro
    de uma variável algo que
    não foi setado
*/
const {
    conta: {
        ag,
        num,
    }
} = pessoa
console.log(num, ag)