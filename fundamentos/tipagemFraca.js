/*
    Javascript é uma linguagem
    de tipagem dinâmica (fraca)
*/    

let qualquer = 'Legal'
console.log(qualquer)

console.log(typeof qualquer)
/*
    Consulta o tipo
    da variável
*/

qualquer = 3.1516
console.log(qualquer)
console.log(typeof qualquer)

/*
    Evitar utilizar nomes
    genéricos e singlas
    para definir suas variáveis
*/    