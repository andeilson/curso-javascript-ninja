/*
    Qualquer número divido por 0
    não gera erro, e sim o 
    Infinity
*/    
console.log(7 / 0)

console.log('10' / 2)
console.log('Show' * 2)
console.log(0.1 + 0.7)
console.log((10.345).toFixed(2))