/*
    A variável var é visível
    através do console.log
    mesmo não entendo do mesmo bloco
    de código, exceto quando se trata
    de uma função.
*/    

{
    {
        {
            {
                var sera = 'Será???'
                
            }
        }
    }
}

console.log(sera)

function teste() {
    var local = 123
}
teste()
// console.log(local) // não irá ler. A var está
// dentro de uma função, e não no window

