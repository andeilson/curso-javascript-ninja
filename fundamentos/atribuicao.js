const a = 7
let b = 3

/* 
    O símbolo da 
    atribuição vem sempre antes
*/     

b += a // b = b + a
console.log(b)

b -= 4 // b = b - 4
console.log(b)

b *= 2 // b = b * 2
console.log(b)

b /= 2 // b = b / 2
console.log(b)

b %= 2 // b = b % 2 => a sobra caso ocorra em uma divisão
console.log(b)