const peso1 = 1.0
const peso2 = Number('2.0');

console.log(peso1, peso2)

/*
Descobrir se o
valor é inteiro
*/

console.log(Number.isInteger(peso1))
console.log(Number.isInteger(peso2))

const avaliacao1 = 9.871
const avaliacao2 = 6.871

const total = avaliacao1 * peso1 + avaliacao2 * peso2
const media = total / (peso1 + peso2)

console.log(media)

/* 
    Conveter a exibição
    dos números totais de uma operação
    aritmética
*/
console.log(media.toFixed(2))

/* 
    Converter um valor
    numérico para uma
    string
*/
console.log(media.toString())

/* 
    Converter um valor
    numérico para uma
    string e binário
*/
console.log(media.toString(2))

console.log(typeof media) 