// Armazenando uma função em uma variável

const imprimirSoma = function (a, b) {
    console.log(a + b)
}
imprimirSoma(2, 3)

// Armazenando uma função arrow em uma variável

const soma = (a, b) => {
    return a + b
}
console.log(soma(2, 3))

// Retorno implícito

const subtracao = (a, b) => a - b
console.log(subtracao(10, 5))

// Exercícios

function somaIdade (c, d) {
    return c + d
}
console.log(somaIdade(30, 40))


const somaBiscoitos = (e, f) => {
    return e + f
}
console.log(somaBiscoitos(100, 100))

const subtrairBiscoitos = (g, h) => g -h
console.log(subtrairBiscoitos(300, 500))

const imprimir2 = a => console.log(a)
imprimir2('Legal!!!');