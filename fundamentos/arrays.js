const valores = [7.7, 8.9, 6.3, 9.2]
console.log(valores[0], valores[3])

/* 
    Arrays acusam como
    undefined quando não encontra
    o index desejado
*/
console.log(valores[0], valores[4])

/* 
    Você pode incluir um array
    após ele ser declarado.
    Dessa forma, ele já irá aparecer
    na constante.
*/
valores[4] = 10
console.log(valores)

/* 
    Se você incluir um array
    pulando indíces, ele irá acusar
    que existem itens vazios entre o 
    array X e Y
*/
valores[10] = 10

/* 
    Verifica quantos elementos
    existem dentro do array
    (incluindo os vazios caso possuam)
*/
console.log(valores.length)

/* 
    Você consegue dar um 'push'
    direto no array
*/
valores.push({id: 3}, false, null, 'tempo é dinheiro')
console.log(valores)

/* 
    Retira o último valor do 
    array utilizando 'pop'
*/
console.log(valores.pop())

/* 
    Retira o último valor do 
    array utilizando delete
*/
delete valores[0]
console.log(valores)

/* 
    Array é do tipo object
*/
console.log(typeof valores)