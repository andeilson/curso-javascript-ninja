/* 
    Variáveis definidas
    com o escopo var tem escopo global, e função;
    E variáveis com let escopo global, e função
    escopo de bloco;
    O código irá procurar primeiramente
    dentro de chaves, e depois fora.
*/

var numero = 1
{
    let numero = 2
    console.log('dentro =', numero)
}

console.log('fora =', numero)