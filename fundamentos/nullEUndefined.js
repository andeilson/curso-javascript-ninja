const a = {name: 'Teste'}
console.log(a)

/*
    Como b armazena um endereço
    onde a armazena o mesmo endereço
    os endereços igualam e o valor de b
    irá 'sobrepor' o valor de a.
    É conhecido como atribuição por
    referência.
*/
const b = a
b.name = 'Opa'
console.log(a, b)


/*
    Quando se trabalha com tipos
    primitivos, você trabalha com 
    cópia por valor.
*/
let c = 3
let d = c
d++

console.log(c, d)


// Null

/* 
    is not defined é quando
    uma variável não foi declarado.
    undefined, e quando você não inicializa
    a variável
*/    
let valor // não inicializada
console.log(valor)

/*
    null significa que se você tem uma variável
    ela não está apontando para nenhum endereço
    de memória.
    null => ausência de valor
*/    
valor = null
console.log(valor)
// console.log(valor.toString()) // Erro!

const produto = {}
console.log(produto.preco)
console.log(produto)

produto.preco = 3.50
console.log(produto)

produto.preco = undefined // evitar atribuir undefined
console.log(!!produto.preco)
// delete produto.preco
console.log(produto)

produto.preco = null // sem valor
console.log(!!produto.preco)
console.log(produto)