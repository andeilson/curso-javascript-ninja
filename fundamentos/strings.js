const escola = "Cod3r"

/* 
    Realiza a consulta
    de um caractere dentro
    de uma string
*/
    
console.log(escola.charAt(4))

/* 
    Realiza a consulta
    de um caractere dentro
    de uma string. Caso não
    exista, ele não retorna nada.
*/
console.log(escola.charAt(5))

/* 
    Realiza a consulta
    de um caractere dentro
    de uma string e retorna
    baseado na tabela ASC/Unicode
*/
console.log(escola.charCodeAt(3))

/* 
    Realiza a consulta
    de um caractere dentro
    de uma string e o indice
    na qual se encontra
*/
console.log(escola.indexOf(3))

/* 
    Começa a exibir o resultado
    a partir do substring informado
*/
console.log(escola.substring(1))

/* 
    Começa a exibir o resultado
    a partir do substring informado e
    quantos caracteres deve retornar
*/
console.log(escola.substring(0, 3))

/* 
    Concatenar uma
    String
*/
console.log('Escola '.concat(escola).concat('!'))

/* 
    Substituir informação
    dentro de uma string
*/
console.log(escola.replace(3, 'e'));

/* 
    Pega a cadeia de caracteres
    e joga dentro de um array
*/
console.log('Ana,Andeilson,Melissa'.split(','))