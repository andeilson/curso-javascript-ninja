const fabricantes = ['Mercedes', 'Audi', 'BMW']

function imprimir(nome, indice) {
    console.log(`${indice + 10} = ${nome}`)
}

fabricantes.forEach(imprimir)
// fabricantes.forEach(function(fabricante) {
//     console.log(fabricante)
// })
// fabricantes.forEach(fabricante => console.log(fabricante))