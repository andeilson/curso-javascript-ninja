class Pessoa {
    constructor(nome, idade, profissao) {
        this.nome = nome
        this.idade = idade
        this.profissao = profissao
    }
    falar() {
        console.log(`Meu nome é ${this.nome}, tenho ${this.idade} e sou ${this.profissao}`)
    }
}

const p1 = new Pessoa('Andeilson', 29, 'Programador')
p1.falar()

const criarPessoa = nome => {
    return {
        falar: () => console.log(`Meu nome é ${nome}`)
    }
}

const p2 = criarPessoa('João')
p2.falar()