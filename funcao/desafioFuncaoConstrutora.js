function Pessoa(nome, idade, profissao) {
    this.nome = nome
    this.idade = idade
    this.profissao = profissao

    this.falar = function () {
        console.log(`Meu nome é ${this.nome}, tenho ${this.idade} anos e sou ${this.profissao}`)
    }
}

const p1 = new Pessoa('Andeilson', 29, 'Programador')
p1.falar(p1.nome)