const pilotos = ['Vettel', 'Alonso', 'Raikkonen', 'Massa']
pilotos.pop() // remove o último elemento do array 

console.log(pilotos)

pilotos.push('Verstappen') // adiciona no último elemento do array
console.log(pilotos)

pilotos.shift() // remove o primeiro elemento do array
console.log(pilotos)

pilotos.unshift('Hamilton') // insere um novo elemento no início do array
console.log(pilotos)

// splice pode adicionar e remover elementos

// adicionar
pilotos.splice(2, 0, 'Bottas', 'Massa')
console.log(pilotos)

// remover

pilotos.splice(3, 1) // remover o terceiro elemento
console.log(pilotos)

const algunsPilotos1 = pilotos.slice(2) // novo array de acordo com uma fatia
console.log(algunsPilotos1)

const algunsPilotos2 = pilotos.slice(1, 4) // novo array de acordo com uma fatia
console.log(algunsPilotos2)